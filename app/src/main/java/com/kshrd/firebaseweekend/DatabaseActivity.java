package com.kshrd.firebaseweekend;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.facebook.login.LoginManager;
import com.firebase.ui.database.FirebaseListAdapter;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.kshrd.firebaseweekend.entity.Article;

import io.fabric.sdk.android.Fabric;

public class DatabaseActivity extends AppCompatActivity {

    FirebaseDatabase firebaseDatabase;
    DatabaseReference articleRef;

    private ListView lvArticle;
    private FirebaseListAdapter<Article> mAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_main);

        firebaseDatabase = FirebaseDatabase.getInstance();
        articleRef = firebaseDatabase.getReference().child("articles");

        lvArticle = (ListView) findViewById(R.id.lvArticle);

//        String key = articleRef.push().getKey();
//        articleRef
//                .child(key)
//                .setValue(new Article(key, "Win or Lose", "Double reward"));

        // Custom Query
        Query query = articleRef.orderByChild("description").equalTo("Coming soon");


        mAdapter = new FirebaseListAdapter<Article>(
                this, Article.class, android.R.layout.two_line_list_item, articleRef
        ) {
            @Override
            protected void populateView(View view, Article article, int position) {
                ((TextView) view.findViewById(android.R.id.text1)).setText(article.getTitle());
                ((TextView) view.findViewById(android.R.id.text2)).setText(article.getDescription());
            }
        };

        lvArticle.setAdapter(mAdapter);

        lvArticle.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                String key = mAdapter.getRef(i).getKey();
                articleRef.child(key).addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        Article article = dataSnapshot.getValue(Article.class);
                        Toast.makeText(DatabaseActivity.this, article.getTitle(), Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });

            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.item, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.logout){
            FirebaseAuth.getInstance().signOut();
            if (LoginManager.getInstance() != null){
                LoginManager.getInstance().logOut();
            }
            Intent intent = new Intent(DatabaseActivity.this, LoginActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        } else if (item.getItemId() == R.id.error){
            throw new RuntimeException("This is a crash");
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mAdapter.cleanup();
    }
}
